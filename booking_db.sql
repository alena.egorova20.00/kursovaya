-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 03 2024 г., 20:32
-- Версия сервера: 10.8.4-MariaDB
-- Версия PHP: 8.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `booking_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `bookings`
--

CREATE TABLE `bookings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `room_id` bigint(20) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `confirmation_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('pending','confirmed','cancelled','requested') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `bookings`
--

INSERT INTO `bookings` (`id`, `user_id`, `room_id`, `full_name`, `phone`, `email`, `date_from`, `date_to`, `confirmation_code`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-12', '2024-05-15', '307839', 'pending', '2024-05-03 13:33:05', '2024-05-03 13:33:05'),
(2, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-12', '2024-05-15', '645402', 'confirmed', '2024-05-03 13:33:07', '2024-05-03 13:33:18'),
(3, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-12', '2024-05-13', '995505', 'pending', '2024-05-03 13:35:44', '2024-05-03 13:35:44'),
(4, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-12', '2024-05-13', '890860', 'pending', '2024-05-03 13:35:45', '2024-05-03 13:35:45'),
(5, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-12', '2024-05-13', NULL, 'pending', '2024-05-03 13:39:02', '2024-05-03 13:39:02'),
(6, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-12', '2024-05-13', NULL, 'pending', '2024-05-03 13:39:12', '2024-05-03 13:39:12'),
(7, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-16', '2024-05-18', '825625', 'pending', '2024-05-03 13:39:48', '2024-05-03 13:39:48'),
(8, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-16', '2024-05-18', '373467', 'confirmed', '2024-05-03 13:41:07', '2024-05-03 13:41:16'),
(9, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-16', '2024-05-18', NULL, 'pending', '2024-05-03 13:41:21', '2024-05-03 13:41:21'),
(10, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-16', '2024-05-18', NULL, 'pending', '2024-05-03 13:46:07', '2024-05-03 13:46:07'),
(11, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-15', '2024-05-16', NULL, 'pending', '2024-05-03 13:46:23', '2024-05-03 13:46:23'),
(12, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-15', '2024-05-16', NULL, 'pending', '2024-05-03 13:48:37', '2024-05-03 13:48:37'),
(13, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-12', '2024-05-13', NULL, 'pending', '2024-05-03 13:48:50', '2024-05-03 13:48:50'),
(14, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-12', '2024-05-13', NULL, 'pending', '2024-05-03 13:49:07', '2024-05-03 13:49:07'),
(15, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-15', '2024-05-16', NULL, 'pending', '2024-05-03 13:53:01', '2024-05-03 13:53:01'),
(16, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-15', '2024-05-16', NULL, 'pending', '2024-05-03 13:53:42', '2024-05-03 13:53:42'),
(17, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-12', '2024-05-14', NULL, 'pending', '2024-05-03 13:53:55', '2024-05-03 13:53:55'),
(18, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-12', '2024-05-14', NULL, 'pending', '2024-05-03 13:53:56', '2024-05-03 13:53:56'),
(19, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-12', '2024-05-14', NULL, 'pending', '2024-05-03 13:53:58', '2024-05-03 13:53:58'),
(20, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-12', '2024-05-14', NULL, 'pending', '2024-05-03 14:00:34', '2024-05-03 14:00:34'),
(21, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-12', '2024-05-14', NULL, 'pending', '2024-05-03 14:00:37', '2024-05-03 14:00:37'),
(22, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-18', '2024-05-25', NULL, 'pending', '2024-05-03 14:02:50', '2024-05-03 14:02:50'),
(23, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-20', '2024-05-25', '583964', 'confirmed', '2024-05-03 14:02:55', '2024-05-03 14:03:06'),
(24, 1, 1, 'Алёна Фёдоровна Егорова', '+79116374773', 'alena.egorova20.00@mail.ru', '2024-05-12', '2024-05-15', NULL, 'pending', '2024-05-03 14:07:43', '2024-05-03 14:07:43');

-- --------------------------------------------------------

--
-- Структура таблицы `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `hotels`
--

CREATE TABLE `hotels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `hotels`
--

INSERT INTO `hotels` (`id`, `name`, `address`, `description`, `created_at`, `updated_at`) VALUES
(1, 'GRAND - HOTEL', 'г. Санкт-Петербург', 'Санкт-Петербург', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2024_04_10_173027_create_hotels_table', 1),
(6, '2024_04_10_173028_create_room_types_table', 1),
(7, '2024_04_10_173029_create_rooms_table', 1),
(8, '2024_04_10_173030_create_bookings_table', 1),
(9, '2024_04_14_173228_add_additional_fields_to_users_table', 1),
(10, '2024_04_17_091810_add_role_to_users_table', 1),
(11, '2024_04_17_131337_create_options_table', 1),
(12, '2024_04_18_100738_add_booked_dates_to_rooms_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `options`
--

CREATE TABLE `options` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `options`
--

INSERT INTO `options` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Кондиционер', '2024-05-03 12:47:26', '2024-05-03 12:47:26'),
(2, 'Дети', '2024-05-03 12:47:32', '2024-05-03 12:47:32'),
(3, 'Животные', '2024-05-03 12:47:39', '2024-05-03 12:47:39'),
(4, 'Wi-Fi', '2024-05-03 12:47:47', '2024-05-03 12:47:47');

-- --------------------------------------------------------

--
-- Структура таблицы `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `rooms`
--

CREATE TABLE `rooms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hotel_id` bigint(20) UNSIGNED NOT NULL,
  `room_type_id` bigint(20) UNSIGNED NOT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `count_guest` int(11) NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('available','booked','cleaning') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'available',
  `options` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`options`)),
  `gallery` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`gallery`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `booked_dates` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`booked_dates`))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `rooms`
--

INSERT INTO `rooms` (`id`, `hotel_id`, `room_type_id`, `number`, `count_guest`, `title`, `description`, `status`, `options`, `gallery`, `created_at`, `updated_at`, `booked_dates`) VALUES
(1, 1, 1, '1', 2, 'Стандартный номер с видом во двор отеля', 'Комфортабельное и уютное пространство, предназначенное для временного проживания гостей. В номере есть всё необходимое для приятного и спокойного отдыха: удобная кровать, шкаф для хранения вещей, рабочий стол, телевизор и собственная ванная комната с душем.\r\nНомер оформлен в традиционном альпийском стиле с элементами домашнего уюта. В интерьере присутствуют тёплые тона и натуральные материалы, что создаёт атмосферу спокойствия и гармонии.', 'available', '[\"1\",\"4\"]', '[\"1714751523_mark-champs-Id2IIl1jOB0-unsplash.jpg\",\"1714751523_toa-heftiba-FV3GConVSss-unsplash.jpg\"]', '2024-05-03 12:52:03', '2024-05-03 14:03:06', '[\"2024-05-12\",\"2024-05-13\",\"2024-05-14\",\"2024-05-15\",\"2024-05-16\",\"2024-05-17\",\"2024-05-18\",\"2024-05-20\",\"2024-05-21\",\"2024-05-22\",\"2024-05-23\",\"2024-05-24\",\"2024-05-25\"]'),
(2, 1, 2, '2', 4, 'Семейный номер', 'Комфортабельное и просторное помещение, предназначенное для размещения семьи или группы друзей. В номере есть две отдельные комнаты, каждая со своей ванной комнатой, что обеспечивает уединение и приватность.\r\nГостиная зона оборудована мягким диваном, журнальным столиком и телевизором, создавая идеальное место для отдыха и развлечений. В номере также имеется полностью оборудованная кухня, где гости могут самостоятельно готовить еду или разогревать готовые блюда.', 'available', '[\"1\",\"2\",\"4\"]', '[\"1714753021_dave-photoz-6MYm-cUJRB0-unsplash.jpg\"]', '2024-05-03 12:54:30', '2024-05-03 13:17:01', NULL),
(3, 1, 3, '3', 2, 'Люксовый номер', 'В люксовом номере вы найдёте изысканную мебель, дорогие ткани, произведения искусства и другие предметы роскоши. В спальне установлена большая кровать с ортопедическим матрасом и постельным бельём из натуральных материалов. В гостиной зоне есть диван, кресла, журнальный столик и телевизор с плоским экраном. В кабинете можно поработать за компьютером или ноутбуком.', 'available', '[\"1\",\"4\"]', '[\"1714751739_roberto-nickson-emqnSQwQQDo-unsplash.jpg\"]', '2024-05-03 12:55:39', '2024-05-03 12:55:39', NULL),
(4, 1, 1, '4', 2, 'Стандартный номер с видом на центр города', 'В номере есть всё необходимое для приятного и спокойного отдыха: удобная кровать, шкаф для хранения вещей, рабочий стол, телевизор и собственная ванная комната с душем.\r\nНомер оформлен в традиционном городском стиле с элементами домашнего уюта. В интерьере присутствуют тёплые тона и натуральные материалы, что создаёт атмосферу спокойствия и гармонии. Из окон открывается вид на центральные достопримечательности города, что позволяет гостям наслаждаться красотой городской архитектуры и ощущать себя частью истории места.', 'available', '[\"1\",\"4\"]', '[\"1714751862_toa-heftiba-FV3GConVSss-unsplash.jpg\"]', '2024-05-03 12:57:42', '2024-05-03 12:57:42', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `room_types`
--

CREATE TABLE `room_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `room_types`
--

INSERT INTO `room_types` (`id`, `name`, `description`, `price`, `created_at`, `updated_at`) VALUES
(1, 'Стандарт', 'В номере есть всё необходимое для приятного и спокойного отдыха: удобная кровать, шкаф для хранения вещей, рабочий стол, телевизор и собственная ванная комната с душем. В номере регулярно проводится уборка, а также предоставляется набор полотенец и средств гигиены.', '5000', '2024-05-03 12:48:36', '2024-05-03 12:48:50'),
(2, 'Семейный', 'В номере есть две отдельные комнаты, каждая со своей ванной комнатой, что обеспечивает уединение и приватность. Гостиная зона оборудована мягким диваном, журнальным столиком и телевизором, создавая идеальное место для отдыха и развлечений. В номере также имеется полностью оборудованная кухня, где гости могут самостоятельно готовить еду или разогревать готовые блюда.', '8500', '2024-05-03 12:49:33', '2024-05-03 12:49:33'),
(3, 'Люкс', 'Просторный и светлый номер оформлен в элегантном современном стиле с использованием дорогих материалов и изысканных предметов интерьера. В номере есть большая двуспальная кровать с ортопедическим матрасом и пуховыми подушками, обеспечивающая комфортный сон. Ванная комната отделана мрамором и оснащена душевой кабиной, ванной, раковиной и биде.', '12500', '2024-05-03 12:50:38', '2024-05-03 12:50:38');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patronymic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `surname`, `patronymic`, `phone`, `avatar`, `role`) VALUES
(1, 'Алёна', 'alena.egorova20.00@mail.ru', NULL, '$2y$12$lXvQftow5dD4zmyYkAEhZO6HlAYfx/KraHmEpdDsPcyMZ/6yjhyX6', '8LaykoEKSkUpa7YNeaCoPLMaGA2TH9bkn7ETkAY1pidRrnD8nPcjqcF7whYi', '2024-05-03 12:12:26', '2024-05-03 12:24:23', 'Егорова', 'Фёдоровна', '+79116374773', 'avatars/r9FDO5CITngwEoE9gTFSOKEzsQHwENGzacxN2oth.jpg', 'admin');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookings_user_id_foreign` (`user_id`),
  ADD KEY `bookings_room_id_foreign` (`room_id`);

--
-- Индексы таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Индексы таблицы `hotels`
--
ALTER TABLE `hotels`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Индексы таблицы `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Индексы таблицы `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rooms_hotel_id_foreign` (`hotel_id`),
  ADD KEY `rooms_room_type_id_foreign` (`room_type_id`);

--
-- Индексы таблицы `room_types`
--
ALTER TABLE `room_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT для таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `hotels`
--
ALTER TABLE `hotels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `options`
--
ALTER TABLE `options`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `room_types`
--
ALTER TABLE `room_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `bookings`
--
ALTER TABLE `bookings`
  ADD CONSTRAINT `bookings_room_id_foreign` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `bookings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_hotel_id_foreign` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `rooms_room_type_id_foreign` FOREIGN KEY (`room_type_id`) REFERENCES `room_types` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
