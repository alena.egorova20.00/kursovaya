import {
    defineConfig
} from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/css/app.css',
                'resources/js/app.js',

                // ---

                "resources/scss/layouts/common.scss",
                "resources/js/layouts/common.js",

                "resources/scss/main.scss",
                "resources/js/main.js",

                "resources/scss/viewRooms.scss",
                "resources/js/viewRooms.js",

                "resources/scss/rooms.search.scss",
                "resources/js/rooms.search.js",

                "resources/scss/rooms.show.scss",
                "resources/js/rooms.show.js",

                "resources/scss/booking.create.scss",
                "resources/js/booking.create.js",

                "resources/scss/profile.bookings.scss",
                "resources/js/profile.bookings.js",

                "resources/scss/profile.edit.scss",
                "resources/js/profile.edit.js",

                "resources/scss/about.scss",
                "resources/js/about.js",

                // admin

                "resources/scss/admin.dashboard.scss",
                "resources/js/admin.dashboard.js",

                "resources/scss/admin.rooms.index.scss",
                "resources/js/admin.rooms.index.js",

                "resources/scss/admin.rooms.create.scss",
                "resources/js/admin.rooms.create.js",

                "resources/scss/admin.rooms.edit.scss",
                "resources/js/admin.rooms.edit.js",

                "resources/scss/admin.bookings.index.scss",
                "resources/js/admin.bookings.index.js",

                "resources/scss/admin.room_type.index.scss",
                "resources/js/admin.room_type.index.js",

                "resources/scss/admin.room_type.create.scss",
                "resources/js/admin.room_type.create.js",

                "resources/scss/admin.room_type.edit.scss",
                "resources/js/admin.room_type.edit.js",

                "resources/scss/admin.users.index.scss",
                "resources/js/admin.users.index.js",

                "resources/scss/admin.options.index.scss",
                "resources/js/admin.options.index.js",

                "resources/scss/admin.options.create.scss",
                "resources/js/admin.options.create.js",

                "resources/scss/admin.options.edit.scss",
                "resources/js/admin.options.edit.js",
            ],
            refresh: true,
        }),
    ],
});