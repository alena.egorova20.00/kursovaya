<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = [
        'user_id', 'room_id', 'full_name', 'phone', 'email', 'date_from', 'date_to', 'confirmation_code', 'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // Связь "Принадлежит к комнате"
    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    // Метод для генерации и сохранения кода подтверждения
    public function generateConfirmationCode()
    {
        $this->confirmation_code = mt_rand(100000, 999999);
        $this->save();
    }
}
