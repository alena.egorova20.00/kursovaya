<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
        'number',
        'room_type_id',
        'title',
        'description',
        'options',
        'gallery',
        'status',
        'booked_dates',
        'count_guest',
    ];


    protected $casts = [
        'booked_dates' => 'array',
    ];

    public function room_type()
    {
        return $this->belongsTo(RoomType::class);
    }

    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }

    public function hasOption($option)
    {
        $options = json_decode($this->options, true);

        return isset($options[$option]) && $options[$option];
    }
    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

}
