<?php

namespace App\Http\Controllers;

use App\Models\Room;
use App\Models\RoomType;
use Illuminate\Http\Request;
use App\Models\Option;

class AdminRoomController extends Controller
{
    public function index()
    {
        $rooms = Room::all();
        return view('admin.rooms.index', compact('rooms'));
    }

    public function create()
    {
        $options = Option::all();
        $roomTypes = RoomType::all();
        return view('admin.rooms.create', compact('options', 'roomTypes'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'number' => 'required|string',
            'room_type_id' => 'required|exists:room_types,id',
            'title' => 'required|string',
            'description' => 'required|string',
            'options' => 'array',
            'gallery' => 'nullable|array',
            'gallery.*' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
            'count_guest' => 'required|integer'
        ]);

        $options = [];
        if (isset($validatedData['options'])) {
            $options = json_encode($validatedData['options']);
        }

        $room = new Room();
        $room->hotel_id = 1;
        $room->number = $validatedData['number'];
        $room->room_type_id = $validatedData['room_type_id'];
        $room->title = $validatedData['title'];
        $room->description = $validatedData['description'];
        $room->count_guest = $validatedData['count_guest'];
        $room->options = $options;

        $room->save();

        // Создание директории для данной комнаты, если она еще не существует
        $directory = public_path('uploads/' . $room->id);
        if (!file_exists($directory)) {
            mkdir($directory, 0755, true);
        }

        // Сохранение изображений галереи, если они загружены
        if ($request->hasFile('gallery')) {
            $gallery = [];
            foreach ($request->file('gallery') as $image) {
                // Генерация имени файла
                $imageName = time() . '_' . $image->getClientOriginalName();

                // Сохранение изображения в директории комнаты
                $image->move($directory, $imageName);

                // Добавление имени файла в массив галереи
                $gallery[] = $imageName;
            }
            // Сохранение путей к изображениям в базе данных
            $room->gallery = json_encode($gallery);
        }


        $room->save();

        return redirect()->route('admin.rooms.index')->with('success', 'Номер комнаты успешно добавлен.');
    }

    public function show(Room $room)
    {
        return view('admin.rooms.show', compact('room'));
    }

    public function edit(Room $room)
    {
        $options = Option::all();
        $roomTypes = RoomType::all();
        return view('admin.rooms.edit', compact('room', 'roomTypes', 'options'));
    }

    public function update(Request $request, Room $room)
    {
        $validatedData = $request->validate([
            'number' => 'required|string',
            'room_type_id' => 'required|exists:room_types,id',
            'title' => 'required|string',
            'description' => 'required|string',
            'options' => 'array',
            'gallery' => 'nullable|array',
            'gallery.*' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
            'count_guest' => 'required|integer'
        ]);

        $options = [];
        if (isset($validatedData['options'])) {
            $options = json_encode($validatedData['options']);
        }

        // Обновление информации о номере комнаты
        $room->update([
            'number' => $validatedData['number'],
            'room_type_id' => $validatedData['room_type_id'],
            'title' => $validatedData['title'],
            'description' => $validatedData['description'],
            'options' => $options,
            'count_guest' => $validatedData['count_guest'],
            'gallery' => $validatedData['gallery']
        ]);

        // Обновление изображений галереи, если они загружены
        if ($request->hasFile('gallery')) {

            // Создание директории для данной комнаты, если она еще не существует
            $directory = public_path('uploads/' . $room->id);
            if (!file_exists($directory)) {
                mkdir($directory, 0755, true);
            }

            $gallery = [];
            foreach ($request->file('gallery') as $image) {
                // Генерация имени файла
                $imageName = time() . '_' . $image->getClientOriginalName();

                // Сохранение изображения в директории комнаты
                $image->move($directory, $imageName);

                // Добавление имени файла в массив галереи
                $gallery[] = $imageName;
            }
            // Сохранение путей к изображениям в базе данных
            $room->gallery = json_encode($gallery);
        }

        $room->save();

        return redirect()->route('admin.rooms.index')->with('success', 'Информация о номере комнаты успешно обновлена.');
    }

    public function destroy(Room $room)
    {
        $room = Room::findOrFail($room->id);

        $room->delete();

        return redirect()->route('admin.rooms.index')->with('success', 'Номер комнаты успешно удален.');
    }

}
