<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RoomType;

class AdminRoomTypeController extends Controller
{
    public function index()
    {
        $roomTypes = RoomType::all();
        return view('admin.room_type.index', compact('roomTypes'));
    }

    public function create()
    {
        return view('admin.room_type.create');
    }

    public function store(Request $request)
    {
        // Валидация входящих данных
        $validatedData = $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|integer'
        ]);

        // Создание нового типа номера
        $roomType = new RoomType();
        $roomType->name = $validatedData['name'];
        $roomType->description = $validatedData['description'];
        $roomType->price = $validatedData['price'];
        $roomType->save();

        // Перенаправление пользователя после сохранения
        return redirect()->route('admin.room_type.index')->with('success', 'Тип номера успешно добавлен.');
    }

    public function show(RoomType $room_type)
    {
        return view('admin.room_type.show', compact('room_type'));
    }

    public function edit(RoomType $room_type)
    {
        return view('admin.room_type.edit', compact('room_type'));
    }

    public function update(Request $request, RoomType $room_type)
    {
        // Валидация входящих данных
        $validatedData = $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|integer'
        ]);

        // Обновление информации о типе номера
        $room_type->update([
            'name' => $validatedData['name'],
            'description' => $validatedData['description'],
            'price' => $validatedData['price'],
        ]);

        // Перенаправление пользователя после обновления
        return redirect()->route('admin.room_type.index')->with('success', 'Информация о типе номера успешно обновлена.');
    }

    public function destroy(RoomType $roomType)
    {
        if (!$roomType) {
            return redirect()->route('admin.room_type.index')->with('error', 'Тип номера не найден.');
        }

        $roomType->delete();

        return redirect()->route('admin.room_type.index')->with('success', 'Тип номера успешно удален.');
    }
}
