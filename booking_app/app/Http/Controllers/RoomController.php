<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Room;

class RoomController extends Controller
{
    public function show($id)
    {
        $room = Room::where('status', 'available')->findOrFail($id);
        return view('rooms.show', compact('room'));
    }
}
