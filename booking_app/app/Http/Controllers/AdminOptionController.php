<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Option;

class AdminOptionController extends Controller
{
    public function index()
    {
        $options = Option::all();
        return view('admin.options.index', compact('options'));
    }

    public function create()
    {
        return view('admin.options.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|unique:options',
        ]);

        $option = new Option();
        $option->name = $validatedData['name'];
        $option->save();

        return redirect()->route('admin.options.index')->with('success', 'Опция успешно добавлена.');
    }

    public function edit(Option $option)
    {
        return view('admin.options.edit', compact('option'));
    }

    public function update(Request $request, Option $option)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|unique:options,name,' . $option->id,
        ]);

        $option->update([
            'name' => $validatedData['name'],
        ]);

        return redirect()->route('admin.options.index')->with('success', 'Опция успешно обновлена.');
    }

    public function destroy(Option $option)
    {
        $option->delete();

        return redirect()->route('admin.options.index')->with('success', 'Опция успешно удалена.');
    }
}
