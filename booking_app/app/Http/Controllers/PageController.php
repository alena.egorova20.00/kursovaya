<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Room;
use App\Models\Option;
use Carbon\Carbon;

class PageController extends Controller
{
    public function index()
    {
        $options = Option::all();
        $rooms = Room::where('status', 'available')->take(3)->get();

        return view('home.index', compact('options', 'rooms'));
    }

    public function viewRooms()
    {
        $rooms = Room::where('status', 'available')->paginate(3);
        return view('rooms.index', compact('rooms'));
    }

    public function about()
    {
        return view('page.about');
    }

    public function search(Request $request)
    {
        $arrivalDate = $request->input('arrival_date');
        $departureDate = $request->input('departure_date');

        $selectedOptions = $request->input('options');
        $count_guest = $request->input('count_guests');

        $roomsQuery = Room::where('status', 'available');

        // Фильтрация по количеству гостей
        if ($count_guest) {
            $roomsQuery = $roomsQuery->where('count_guest', '>=', $count_guest);
        }

        // Фильтрация по датам заезда и выезда
        if ($arrivalDate && $departureDate) {
            $arrivalDate = Carbon::createFromFormat('Y-m-d', $arrivalDate);
            $departureDate = Carbon::createFromFormat('Y-m-d', $departureDate);

            $bookedRoomIds = Room::whereHas('bookings', function ($query) use ($arrivalDate, $departureDate) {
                $query->where(function ($subQuery) use ($arrivalDate, $departureDate) {
                    $subQuery->where('date_from', '<=', $departureDate)
                            ->where('date_to', '>=', $arrivalDate);
                });
            })->pluck('id')->toArray();

            $roomsQuery->whereNotIn('id', $bookedRoomIds);
        }


        // Фильтрация по выбранным опциям
        if ($selectedOptions) {
            foreach ($selectedOptions as $v) {
                $roomsQuery = $roomsQuery->where("options", "like", "%\"$v\"%");
            }
        }

        // Дополнительные фильтры

        // Получение результатов запроса с пагинацией
        $rooms = $roomsQuery->paginate(3);

        return view('rooms.index', compact('rooms'));
    }




}
