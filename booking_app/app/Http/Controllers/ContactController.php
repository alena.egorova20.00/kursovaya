<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendContactEmail;

class ContactController extends Controller
{
    public function sendEmail(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'phone' => 'required|string',
        ]);

        // Получаем данные из формы
        $name = $request->input('name');
        $phone = $request->input('phone');

        // Отправляем письмо администратору
        Mail::to('alena.egorova.adm@yandex.ru')->send(new SendContactEmail($name, $phone));

        // Возвращаем ответ на клиентскую сторону (например, сообщение об успешной отправке)
        return response()->json(['message' => 'Email sent successfully'], 200);
    }
}
