<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Models\Room;
use App\Models\Booking;

use Illuminate\Support\Facades\Mail;
use App\Mail\BookingConfirmation;

class BookingController extends Controller
{
    public function create($room_id)
    {
        $room = Room::findOrFail($room_id);
        $bookedDates = $room->booked_dates ?? [];

        // Проверка доступности комнаты для бронирования
        if ($room->status !== 'available') {
            return redirect('/');
        }

        return view('booking.create', compact('room', 'bookedDates'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'full_name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'date_from' => 'required|date',
            'date_to' => 'required|date'
        ]);

        // Поиск комнаты
        $room = Room::findOrFail($request->room_id);

        // Проверка доступности комнаты для бронирования
        if ($room->status !== 'available') {
            return response()->json(['error' => 'Комната уже забронирована.']);
        }

        // Создание нового бронирования
        $booking = Booking::create([
            'room_id' => $request->room_id,
            'user_id' => Auth::user()->id,
            'full_name' => $request->full_name,
            'phone' => $request->phone,
            'email' => $request->email,
            'date_from' => $request->date_from,
            'date_to' => $request->date_to,
        ]);


        // Генерация кода подтверждения
        $booking->generateConfirmationCode();

        // Отправка письма с кодом подтверждения
        Mail::to($booking->email)->send(new BookingConfirmation($booking));

        return response()->json([
            'success' => 'Ваше бронирование было успешно создано. Код подтверждения был отправлен на ваш email.',
            'email' => $booking->email
        ]);
    }

    public function confirm(Request $request)
    {
        $booking = Booking::where('confirmation_code', $request->confirmation_code)->first();

        if (!$booking) {
            return response()->json(['error' => 'Неправильный код подтверждения. Пожалуйста, попробуйте еще раз.']);
        }

        if ($booking->status === 'confirmed') {
            return response()->json(['error' => 'Бронирование уже подтверждено.']);
        }

        // Получаем даты начала и окончания бронирования
        $startDate = $booking->date_from;
        $endDate = $booking->date_to;

        // Получаем комнату для бронирования
        $room = $booking->room;

        // Получаем уже забронированные даты для этой комнаты
        $existingBookedDates = $room->booked_dates ?? [];

        // Проверяем, не забронированы ли уже какие-либо даты в указанном периоде
        for ($date = $startDate; $date <= $endDate; $date = date('Y-m-d', strtotime($date . ' +1 day'))) {
            if (in_array($date, $existingBookedDates)) {
                return response()->json(['error' => 'Комната уже забронирована на указанный период.']);
            }
        }
        // Получаем уже забронированные даты для этой комнаты
        $existingBookedDates = $room->booked_dates ?? [];

        // Если все даты доступны, добавляем их в массив забронированных дат
        $bookedDates = [];
        for ($date = $startDate; $date <= $endDate; $date = date('Y-m-d', strtotime($date . ' +1 day'))) {
            $bookedDates[] = $date;
        }

        // Обновляем забронированные даты комнаты
        $room->update(['booked_dates' => array_merge($existingBookedDates, $bookedDates)]);

        // Обновляем статус бронирования на подтвержденный
        $booking->update(['status' => 'confirmed']);

        return response()->json(['success' => 'Бронирование успешно подтверждено!']);
    }





    public function cancel(Request $request, $id)
    {
        // Находим бронирование по его идентификатору
        $booking = Booking::findOrFail($id);

        // Устанавливаем статус бронирования в "отменено"
        $booking->update(['status' => 'cancelled']);

        // Находим связанную с бронированием комнату и устанавливаем ее статус в "доступно"
        $room = $booking->room;
        $room->update(['status' => 'available']);

        // После отмены бронирования возвращаем успешный ответ
        return response()->json(['success' => 'Бронирование успешно отменено.']);
    }

}
