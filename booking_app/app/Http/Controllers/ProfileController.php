<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class ProfileController extends Controller
{
    public function bookings()
    {
       $user = Auth::user();
       $bookings = $user->bookings()
                 ->where('date_to', '>=', now())
                 ->whereIn('status', ['confirmed', 'requested'])
                 ->get();

       $expiredBookings = $user->bookings()
        ->where('date_to', '<', now())
        ->orWhere('status', 'cancelled')
        ->get();

       return view('profile.bookings', compact('bookings', 'expiredBookings'));
    }

    public function edit()
    {
        $user = auth()->user();
        return view('profile.edit', compact('user'));
    }

    public function update(Request $request)
    {
        $user = auth()->user();
        $user->surname = $request->surname;
        $user->name = $request->name;
        $user->patronymic = $request->patronymic;
        $user->email = $request->email;
        $user->phone = $request->phone;
        if ($request->hasFile('avatar')) {
            // Загрузка и обновление изображения профиля
            $avatarPath = $request->file('avatar')->store('avatars', 'public');
            $user->avatar = $avatarPath;
        }
        $user->save();
        return redirect()->route('profile.edit')->with('success', 'Профиль успешно обновлен');
    }





    // public function destroy(Request $request): RedirectResponse
    // {
    //     $request->validateWithBag('userDeletion', [
    //         'password' => ['required', 'current_password'],
    //     ]);
    //
    //     $user = $request->user();
    //
    //     Auth::logout();
    //
    //     $user->delete();
    //
    //     $request->session()->invalidate();
    //     $request->session()->regenerateToken();
    //
    //     return Redirect::to('/');
    // }
}
