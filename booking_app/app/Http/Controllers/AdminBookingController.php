<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Booking;

class AdminBookingController extends Controller
{
    public function index()
    {
        $bookings = Booking::all();
        return view('admin.bookings.index', compact('bookings'));
    }

    public function show(Booking $booking)
    {
        return view('admin.bookings.show', compact('booking'));
    }

    public function confirm(Request $request, Booking $booking)
    {
        // Подтверждение бронирования
        $booking->update(['status' => 'confirmed']);

        return redirect()->route('admin.bookings.index')->with('success', 'Бронирование успешно подтверждено.');
    }

    public function destroy(Booking $booking)
    {
        $booking->delete();

        return redirect()->route('admin.bookings.index')->with('success', 'Бронирование успешно удалено.');
    }

}
