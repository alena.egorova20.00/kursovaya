<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\BookingController;

use App\Http\Controllers\AdminDashboardController;
use App\Http\Controllers\AdminRoomController;
use App\Http\Controllers\AdminBookingController;
use App\Http\Controllers\AdminRoomTypeController;
use App\Http\Controllers\AdminUserController;
use App\Http\Controllers\AdminOptionController;
use App\Http\Controllers\ContactController;

Route::post('/send-email', [ContactController::class, 'sendEmail'])->name('send-email');


Route::get('/email/verify', function (){
    return view('auth.verify-email');
}) -> middleware('auth') -> name('verification.notice');
Route::post('email/verification-notification', function (Request $request){
    $request->user()->sendEmailVerificationNotification();
    return back()->with('message', 'Verification link sent!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');

use Illuminate\Foundation\Auth\EmailVerificationRequest;

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();

    return redirect('main');
})->middleware(['auth', 'signed'])->name('verification.verify');


Route::controller(PageController::class)->group(function () {
    Route::get('/', 'index')->name('main');
    Route::get('/about', 'about')->name('about');
    Route::get('/rooms', 'viewRooms')->name('viewRooms');
    Route::get('/rooms/search', 'search')->name('rooms.search');

});

Route::controller(RoomController::class)->group(function () {
    Route::get('/rooms/{id}', 'show')->name('rooms.show');
});

Route::group(['prefix' => 'booking'], function () {
    Route::get('create/{room_id}', [BookingController::class, 'create'])->name('booking.create');
    Route::post('store', [BookingController::class, 'store'])->name('booking.store');
    Route::post('confirm', [BookingController::class, 'confirm'])->name('booking.confirm');
    Route::post('/{id}/cancel', [BookingController::class, 'cancel'])->name('booking.cancel');
});

Route::controller(ProfileController::class)->group(function () {
	Route::group(['middleware' => 'auth'], function () {
        Route::get('/profile/bookings', 'bookings')->name('profile.bookings');
        Route::get('/profile/edit', 'edit')->name('profile.edit');
        Route::post('/profile/update', 'update')->name('profile.update');
	});
});



// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    // Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {

    Route::get('/dashboard', [AdminDashboardController::class, 'index'])->name('admin.dashboard');

    Route::get('/bookings', [AdminBookingController::class, 'index'])->name('admin.bookings.index');
    Route::post('/bookings/{booking}/confirm', [AdminBookingController::class, 'confirm'])->name('admin.bookings.confirm');
    Route::delete('/bookings/{booking}', [AdminBookingController::class, 'destroy'])->name('admin.bookings.destroy');

    // Маршруты для управления номерами отеля
    Route::get('/rooms', [AdminRoomController::class, 'index'])->name('admin.rooms.index');
    Route::get('/rooms/create', [AdminRoomController::class, 'create'])->name('admin.rooms.create');
    Route::post('/rooms', [AdminRoomController::class, 'store'])->name('admin.rooms.store');
    Route::get('/rooms/{room}/edit', [AdminRoomController::class, 'edit'])->name('admin.rooms.edit');
    Route::put('/rooms/{room}', [AdminRoomController::class, 'update'])->name('admin.rooms.update');
    Route::delete('/rooms/{room}', [AdminRoomController::class, 'destroy'])->name('admin.rooms.destroy');

    // Добавляем маршруты для управления классами отеля
    Route::get('/room_type', [AdminRoomTypeController::class, 'index'])->name('admin.room_type.index');
    Route::get('/room_type/create', [AdminRoomTypeController::class, 'create'])->name('admin.room_type.create');
    Route::post('/room_type', [AdminRoomTypeController::class, 'store'])->name('admin.room_type.store');
    Route::get('/room_type/{room_type}', [AdminRoomTypeController::class, 'show'])->name('admin.room_type.show');
    Route::get('/room_type/{room_type}/edit', [AdminRoomTypeController::class, 'edit'])->name('admin.room_type.edit');
    Route::put('/room_type/{room_type}', [AdminRoomTypeController::class, 'update'])->name('admin.room_type.update');
    Route::delete('/room_type/{room_type}', [AdminRoomTypeController::class, 'destroy'])->name('admin.room_type.destroy');

    Route::get('/users', [AdminUserController::class, 'index'])->name('admin.users.index');


    Route::get('/options', [AdminOptionController::class, 'index'])->name('admin.options.index');
    Route::get('/options/create', [AdminOptionController::class, 'create'])->name('admin.options.create');
    Route::post('/options', [AdminOptionController::class, 'store'])->name('admin.options.store');
    Route::get('/options/{option}/edit', [AdminOptionController::class, 'edit'])->name('admin.options.edit');
    Route::put('/options/{option}', [AdminOptionController::class, 'update'])->name('admin.options.update');
    Route::delete('/options/{option}', [AdminOptionController::class, 'destroy'])->name('admin.options.destroy');
});

require __DIR__.'/auth.php';
