<?php

return [
    'main' => [
        'title' => 'Добро пожаловать в GRAND - HOTEL',
        'description' => 'Исследуйте роскошь и удобства нашего GRAND - HOTEL на главной странице',
    ],
    'about' => [
        'title' => 'История GRAND - HOTEL',
        'description' => 'Узнайте больше о нашем прекрасном GRAND - HOTEL и его истории',
    ],
    'viewRooms' => [
        'title' => 'Очарование номеров в GRAND - HOTEL',
        'description' => 'Погрузитесь в атмосферу роскоши и уюта наших номеров в GRAND - HOTEL',
    ],
    'rooms' => [
        'search' => [
            'title' => 'Поиск идеального номера в GRAND - HOTEL',
            'description' => 'Найдите идеальный номер для вашего пребывания в GRAND - HOTEL с нашим поиском комнат',
        ],
        'show' => [
            'title' => 'Исследуйте комфорт в GRAND - HOTEL',
            'description' => 'Погрузитесь в роскошь и уют конкретного номера в GRAND - HOTEL',
        ],
    ],
    'booking' => [
        'create' => [
            'title' => 'Забронируйте ваше место в GRAND - HOTEL',
            'description' => 'Создайте свой уникальный опыт пребывания, забронировав номер в GRAND - HOTEL',
        ],
        'store' => [
            'title' => 'Сохранение вашего пребывания в GRAND - HOTEL',
            'description' => 'Сохраните ваше бронирование и гарантируйте себе удобное размещение в GRAND - HOTEL',
        ],
        'confirm' => [
            'title' => 'Подтверждение вашего бронирования в GRAND - HOTEL',
            'description' => 'Убедитесь в подтверждении вашего бронирования в GRAND - HOTEL для беззаботного пребывания',
        ],
        'cancel' => [
            'title' => 'Отмена вашего бронирования в GRAND - HOTEL',
            'description' => 'Отмените свое бронирование в GRAND - HOTEL в случае изменения ваших планов',
        ],
    ],
    'profile' => [
        'bookings' => [
            'title' => 'Ваши бронирования в GRAND - HOTEL',
            'description' => 'Просмотрите и управляйте вашими бронированиями в GRAND - HOTEL для незабываемого отдыха',
        ],
        'edit' => [
            'title' => 'Редактирование вашего профиля в GRAND - HOTEL',
            'description' => 'Обновите вашу личную информацию и предпочтения для идеального пребывания в GRAND - HOTEL',
        ],
        'update' => [
            'title' => 'Обновление вашего профиля в GRAND - HOTEL',
            'description' => 'Внесите изменения в ваш профиль в GRAND - HOTEL для улучшения вашего опыта пребывания',
        ],
    ],
];
