@extends('admin.admin')

@section('content')
    <div class="container-fluid">
        <h1 class="h2">Добавить номер</h1>

        <form class="form" action="{{ route('admin.rooms.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="number">Номер комнаты</label>
                <input type="text" name="number" id="number" class="form-control">
            </div>
            <div class="form-group">
                <label for="room_type_id">Тип номера</label>
                <select name="room_type_id" id="room_type_id" class="form-control">
                    @foreach ($roomTypes as $roomType)
                        <option value="{{ $roomType->id }}">{{ $roomType->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="title">Заголовок</label>
                <input type="text" name="title" id="title" class="form-control">
            </div>
            <div class="form-group">
                <label for="description">Описание</label>
                <textarea name="description" id="description" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <label for="count_guest">Количество гостей</label>
                <input type="number" name="count_guest" id="count_guest" class="form-control">
            </div>
            <div class="form-group">
                <label for="options">Опции</label><br>
                @foreach ($options as $option)
                    <label><input type="checkbox" name="options[]" value="{{ $option->id }}"> {{ $option->name }}</label><br>
                @endforeach
            </div>

            <div class="form-group">
                <label for="gallery">Галерея</label>
                <input type="file" name="gallery[]" id="gallery" class="form-control" multiple>
            </div>
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </form>
    </div>
@endsection
