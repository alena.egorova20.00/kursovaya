@extends('admin.admin')

@section('content')
    <div class="container-fluid">
        <h1 class="h2">Номера отеля</h1>

        <!-- Кнопка для создания нового номера -->
        <a href="{{ route('admin.rooms.create') }}" class="btn btn-primary mb-3">Добавить номер</a>

        @if ($rooms->isEmpty())
            <p>Нет доступных номеров.</p>
        @else
            <table class="table">
                <thead>
                    <tr>
                        <th>Идентификатор</th>
                        <th>Тип</th>
                        <th>Описание</th>
                        <th>Количество гостей</th>
                        <th>Цена</th>
                        <th>Действия</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rooms as $room)
                        <tr>
                            <td>{{ $room->id }}</td>
                            <td>{{ $room->room_type->name }}</td>
                            <td>{{ $room->description }}</td>
                            <td>{{ $room->count_guest }}</td>
                            <td>{{ $room->room_type->price }}</td>
                            <td>
                                <a href="{{ route('admin.rooms.edit', $room) }}" class="btn btn-primary btn-sm">Редактировать</a>
                                <form action="{{ route('admin.rooms.destroy', $room) }}" method="POST" class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Вы уверены?')">Удалить</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
@endsection
