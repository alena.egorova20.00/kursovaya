@extends('admin.admin')

@section('content')
    <div class="container-fluid">
        <h1 class="h2">Редактировать номер комнаты</h1>

        <form class="form" action="{{ route('admin.rooms.update', $room) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="number">Номер комнаты</label>
                <input type="text" name="number" id="number" class="form-control" value="{{ $room->number }}">
            </div>
            <div class="form-group">
                <label for="room_type_id">Тип комнаты</label>
                <select name="room_type_id" id="room_type_id" class="form-control">
                    @foreach ($roomTypes as $roomType)
                        <option value="{{ $roomType->id }}" @if($room->room_type_id == $roomType->id) selected @endif>{{ $roomType->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="title">Заголовок</label>
                <input type="text" name="title" id="title" class="form-control" value="{{ $room->title }}">
            </div>
            <div class="form-group">
                <label for="description">Описание</label>
                <textarea name="description" id="description" class="form-control">{{ $room->description }}</textarea>
            </div>
            <div class="form-group">
                <label for="count_guest">Количество гостей</label>
                <input type="number" name="count_guest" id="count_guest" class="form-control" value="{{ $room->count_guest }}">
            </div>
            <!-- Вывод опций -->
            <div class="form-group">
                @php
                    $room_options = json_decode($room->options, true);
                @endphp
                <label for="options">Опции</label><br>
                @foreach ($options as $option)
                    <label><input type="checkbox" name="options[]" value="{{ $option->id }}" @if(in_array($option->id, $room_options)) checked @endif> {{ $option->name }}</label><br>
                @endforeach
            </div>
            <!-- Вывод старой картинки -->
            <div class="form-group">
                <label for="image">Текущее изображение</label>
                @if ($room->gallery)
                    @foreach (json_decode($room->gallery) as $image)
                        <img src="{{ asset('uploads/' . $room->id . '/' . $image) }}" alt="Изображение комнаты" class="img-fluid">
                    @endforeach
                @else
                    <p>Изображение отсутствует</p>
                @endif
            </div>
            <!-- Поле для загрузки нового изображения -->
            <div class="form-group">
                <label for="gallery">Новое изображение</label>
                <input type="file" name="gallery[]" id="gallery" class="form-control-file">
            </div>

            <button type="submit" class="btn btn-primary">Сохранить изменения</button>
        </form>
    </div>
@endsection
