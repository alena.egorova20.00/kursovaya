@extends('admin.admin')

@section('content')
    <div class="container-fluid">
        <h1 class="h2">Добавить новую опцию</h1>

        <form class="form" action="{{ route('admin.options.store') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Название опции</label>
                <input type="text" name="name" id="name" class="form-control">
            </div>
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </form>
    </div>
@endsection
