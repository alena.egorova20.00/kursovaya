@extends('admin.admin')

@section('content')
    <div class="container-fluid">
        <h1 class="h2">Опции</h1>

        <div class="row mb-3">
            <div class="col-md-12">
                <a href="{{ route('admin.options.create') }}" class="btn btn-primary">Добавить новую опцию</a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @if ($options->isEmpty())
                            <p>Нет доступных опций.</p>
                        @else
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Название</th>
                                        <th>Действия</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($options as $option)
                                        <tr>
                                            <td>{{ $option->id }}</td>
                                            <td>{{ $option->name }}</td>
                                            <td>
                                                <a href="{{ route('admin.options.edit', $option) }}" class="btn btn-sm btn-primary">Редактировать</a>
                                                <form action="{{ route('admin.options.destroy', $option) }}" method="POST" class="d-inline">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Вы уверены?')">Удалить</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
