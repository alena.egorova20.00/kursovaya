@extends('admin.admin')

@section('content')
    <div class="container-fluid">
        <h1 class="h2">Редактировать опцию</h1>

        <form class="form" action="{{ route('admin.options.update', $option) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="name">Название опции</label>
                <input type="text" name="name" id="name" class="form-control" value="{{ $option->name }}">
            </div>
            <button type="submit" class="btn btn-primary">Сохранить изменения</button>
        </form>
    </div>
@endsection
