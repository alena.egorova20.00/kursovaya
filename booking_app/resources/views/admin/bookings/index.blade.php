@extends('admin.admin')

@section('content')
    <h1 class="title">Список бронирований</h1>

    @if ($bookings->isEmpty())
        <div class="">
            <p>Нет доступных бронирований.</p>
        </div>
    @else
        <table class="table is-fullwidth">
            <thead>
                <tr>
                    <th>Идентификатор</th>
                    <th>Дата</th>
                    <th>Статус</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($bookings as $booking)
                    <tr>
                        <td>{{ $booking->id }}</td>
                        <td>
                            {{ \Carbon\Carbon::parse($booking->date_from)->format('d.m.Y') }} - {{ \Carbon\Carbon::parse($booking->date_to)->format('d.m.Y') }}
                        </td>
                        <td>{{ $booking->status }}</td>
                        <td>
                            <div class="buttons">
                                {{-- <a class="button is-link" href="{{ route('admin.bookings.show', $booking) }}">Просмотреть</a> --}}
                                <form action="{{ route('admin.bookings.confirm', $booking) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn is-success" onclick="return confirm('Вы уверены?')">Подтвердить</button>
                                </form>
                                <form action="{{ route('admin.bookings.destroy', $booking) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn is-danger" onclick="return confirm('Вы уверены?')">Удалить</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
@endsection
