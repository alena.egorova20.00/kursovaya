<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.head')
<body>
    <div class="admin-wrapper">
        <div class="content admin-content">
            <div class="admin-sidebar">
                <div class="user">
                    @include('svg.icon_admin')
                    <span>
                        @if (Auth::check())
                            {{ Auth::user()->name }} {{ Auth::user()->surname }}
                        @endif
                    </span>

                </div>

                <ul class="nav flex-column">
                    {{-- <li class="nav-item {{ request()->routeIs('admin.dashboard') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.dashboard') }}">
                            Dashboard
                        </a>
                    </li> --}}
                    <li class="nav-item {{ request()->routeIs('admin.rooms.index') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.rooms.index') }}">
                            Номера отеля
                        </a>
                    </li>
                    <li class="nav-item {{ request()->routeIs('admin.bookings.index') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.bookings.index') }}">
                            Бронирования
                        </a>
                    </li>
                    <li class="nav-item {{ request()->routeIs('admin.room_type.index') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.room_type.index') }}">
                            Классы отеля
                        </a>
                    </li>
                    <li class="nav-item {{ request()->routeIs('admin.users.index') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.users.index') }}">
                            Пользователи
                        </a>
                    </li>
                    <li class="nav-item {{ request()->routeIs('admin.options.index') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.options.index') }}">
                            Привилегии номеров
                        </a>
                    </li>
                </ul>

            </div>
            <div class="main-content">
                @yield('content')
            </div>
        </div>
    </div>
</body>
</html>
