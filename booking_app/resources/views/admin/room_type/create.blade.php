@extends('admin.admin')

@section('content')
    <div class="container-fluid">
        <h1 class="h2">Добавить новый тип номера</h1>

        <div class="row">
            <div class="col-md-6">
                <form class="form" action="{{ route('admin.room_type.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Название</label>
                        <input type="text" name="name" id="name" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="description">Описание</label>
                        <textarea name="description" id="description" class="form-control" rows="3" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="price">Цена</label>
                        <input type="number" name="price" id="price" class="form-control" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </form>
            </div>
        </div>
    </div>
@endsection
