@extends('admin.admin')

@section('content')
    <div class="container-fluid">
        <h1 class="h2">Типы номеров</h1>

        <div class="row mb-3">
            <div class="col-md-12">
                <a href="{{ route('admin.room_type.create') }}" class="btn btn-primary">Добавить новый тип номера</a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @if ($roomTypes->isEmpty())
                            <p>Нет доступных типов номеров.</p>
                        @else
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Название</th>
                                        <th>Описание</th>
                                        <th>Цена</th>
                                        <th>Действия</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($roomTypes as $roomType)
                                        <tr>
                                            <td>{{ $roomType->id }}</td>
                                            <td>{{ $roomType->name }}</td>
                                            <td>{{ $roomType->description }}</td>
                                            <td>{{ $roomType->price }} руб.</td>
                                            <td>
                                                <a href="{{ route('admin.room_type.edit', $roomType) }}" class="btn btn-sm btn-primary">Редактировать</a>
                                                <form action="{{ route('admin.room_type.destroy', $roomType) }}" method="POST" class="d-inline">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Вы уверены?')">Удалить</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
