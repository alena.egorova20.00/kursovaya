@extends('admin.admin')

@section('content')
    <div class="container-fluid">
        <h1 class="h2">Редактировать тип номера</h1>

        <form class="form" action="{{ route('admin.room_type.update', $room_type) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="name">Название</label>
                <input type="text" name="name" id="name" class="form-control" value="{{ $room_type->name }}">
            </div>
            <div class="form-group">
                <label for="description">Описание</label>
                <textarea name="description" id="description" class="form-control">{{ $room_type->description }}</textarea>
            </div>
            <div class="form-group">
                <label for="price">Цена</label>
                <input type="number" name="price" id="price" class="form-control" value="{{ $room_type->price }}" required>
            </div>
            <button type="submit" class="btn btn-primary">Сохранить изменения</button>
        </form>
    </div>
@endsection
