@extends('main')

@section('content')

<div class="main__filter">
    <div class="filter-wrapper">
        <form action="{{ route('rooms.search') }}" method="GET" class="filter-form">
            <input class="filter-input" type="text" name="count_guests" placeholder="Кол-во гостей">
            <input class="filter-input" type="date" name="arrival_date" placeholder="Дата заезда">
            <input class="filter-input" type="date" name="departure_date" placeholder="Дата выезда">
            <input class="filter-input" type="text" name="promo_code" placeholder="Промокод">

            <div class="filter-col">
                @foreach ($options as $option)
                    <label>
                        <input type="checkbox" name="options[]" value="{{ $option->id }}">
                        <span>{{ $option->name }}</span>
                    </label>
                @endforeach
            </div>

            <button class="filter-submit" type="submit">Найти номер</button>
        </form>

        <div class="filter-content">
            <p class="filter-title">GRAND HOTEL</p>
            <div class="filter-desc">
                <p>Настоящее время в Санкт-Петербурге</p>
                <span></span>
                <p>Погода в Санкт-Петербурге</p>
            </div>
        </div>
    </div>
</div>

<div class="about_hotel">
    <div class="lf">
        <div class="title">
            Об отеле
        </div>
        <div class="desc">
            GRAND HOTEL — идеальный выбор для комфортного пребывания и отдыха. Отель расположен в самом высоком здании исторического центра города, откуда можно насладиться потрясающим панорамным видом на Санкт-Петербург.
        </div>
        <a href="{{route('about')}}">Подробнее о нас</a>
    </div>
    <div class="rt">
        <img src="{{ asset('img/about_hotel_img.jpg') }}" alt="">
    </div>
</div>

<div class="hotel_rooms">
    <div class="title">
        Номера в отеле
    </div>
    <div class="rooms">
        @if (!empty($room))
            <p>Номера в отеле, к сожалению, отсутсвуют(</p>
        @else
        @foreach($rooms as $room)

            <div class="room_item">

                @if (!empty($room->gallery))
                    @php
                        $gallery = json_decode($room->gallery, true);
                        $imagePath = asset('uploads/' . $room->id . '/' . $gallery[0]);
                    @endphp
                    <img src="{{ $imagePath }}" alt="Room Image">
                @else
                    <p>No image available</p>
                @endif

                <div class="room_title">
                    {{ $room->title }}
                </div>
                <div class="room_desc">
                    {{ $room->description }}
                </div>

                @if ($room->options)
                    <div class="advantages">
                        @php
                            $options = json_decode($room->options);
                        @endphp

                        @foreach ($options as $optionId)
                            @php
                                $option = \App\Models\Option::find($optionId);
                            @endphp

                            @if ($option)
                                <div class="advantage">
                                    @if ($option->name == 'Wi-fi')
                                        @include('svg.icon_wifi')
                                    @elseif ($option->name == 'Кондиционер')
                                        @include('svg.icon_air_conditioner')
                                    @endif
                                    <span>{{ $option->name }}</span>
                                </div>

                            @endif
                        @endforeach
                    </div>
                @endif

                <div class="room_footer">
                    <a href="{{ route('rooms.show', $room->id) }}">
                        @include('svg.icon_room_footer_link')
                    </a>
                </div>
            </div>

        @endforeach
        @endif
    </div>

</div>

<div class="contact_main">
    <div class="lf">
        <div class="title">
            Остались вопросы? Оставьте свои контактные данные и наш менеджер вам перезвонит в течении 15 минут
        </div>
    </div>
    <div class="rt">
        <form class="contact-form" id="contact-form" action="{{ route('send-email') }}" method="post">
            @csrf
            <input type="text" name="name" placeholder="Ваше имя" required>
            <input type="text" name="phone" placeholder="Номер телефона" required>
            <button type="submit" class="submit">ОТПРАВИТЬ</button>
        </form>
    </div>
</div>

@endsection
