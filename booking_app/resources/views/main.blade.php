<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.head')


<body>
    <div class="wrapper">
        @include('layouts.header')

        <div class="content">
            @yield('content')
        </div>

        @include('layouts.footer')
    </div>
</body>
</html>

{{--Модальное окно для авторизации-регистрации--}}

<div class="modal-overflow" id="modal-login">
    <div class="modal-login">
        <span class="modal-close"></span>
        <div class="title">
            Авторизация
        </div>
        <form class="" action="{{ route('login') }}" method="post">
            @csrf
            <input type="email" name="email" value="" placeholder="Email" required>
            <input type="password" name="password" value="" placeholder="Пароль" required>
            <span class="forgot_password" id="forgot_password">Забыли пароль?</span>
            <button type="submit" class="submit">Войти</button>
        </form>

        <span class="no-acc" id="no-acc">Нет аккаунта? <span class="link">Зарегистрироваться</span></span>
    </div>
</div>

<div class="modal-overflow" id="modal-reg">
    <div class="modal-reg">
        <span class="modal-close"></span>
        <div class="title">
            Регистрация
        </div>
        <form action="{{ route('register') }}" method="post">
            @csrf
            <input type="text" name="surname" placeholder="Фамилия" required>
            <input type="text" name="name" placeholder="Имя" required>
            <input type="text" name="patronymic" placeholder="Отчество">
            <input type="text" name="phone" placeholder="Телефон" required>
            <input type="email" name="email" placeholder="Email" required>
            <input type="password" name="password" placeholder="Пароль" required>
            <input type="password" name="password_confirmation" placeholder="Подтвердите пароль" required>
            <div class="polit-row">
                <input type="checkbox" name="agree" required>
                <span>Я даю свое согласие на обработку Персональных данных и согласен с Политикой конфиденциальности</span>
            </div>
            <button type="submit" class="submit">Зарегистрироваться</button>
        </form>
    </div>
</div>

<div class="modal-overflow" id="pass_reset">
    <div class="modal-reset">
        <span class="modal-close"></span>
        <div class="title">
            Восстановление доступа
        </div>

        <form action="{{ route('password.email') }}" method="post">
            @csrf
            <div class="desc">
                Введите ваш адрес электронной почты, мы вам отправим ссылку для восстановления доступа
            </div>
            <input type="email" name="email" placeholder="Электронная почта" required>
            <button type="submit" class="submit">Восстановить доступ</button>
        </form>
    </div>
</div>
