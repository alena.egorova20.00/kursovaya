<header class="header">
    <div class="header__left">
        <a href="{{ url('/') }}" class="header__logo">
            @include('svg.icon_main')
        </a>
    </div>
    <div class="header__center">
        <a href="{{route('viewRooms')}}">Номера отеля</a>
        <a href="{{route('about')}}">О нас</a>

        @if (Auth::check())
            <a href="{{route('profile.bookings')}}">Бронирования</a>
        @else
            <a href="#" class="to-modal">Бронирования</a>
        @endif

    </div>
    <div class="header__right">
        @if (Auth::check())
            <a href="{{route('profile.edit')}}" class="personal-link">
                @include('svg.icon_personal_link')
            </a>
        @else
            <span class="login" id="login">Вход</span>
        @endif
    </div>
</header>
