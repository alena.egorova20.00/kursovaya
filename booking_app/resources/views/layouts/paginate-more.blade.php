@if ($paginator->hasPages())
    <nav class="pagination-main">
        @if ($paginator->hasMorePages())
            <span class="page-link" data-href="{{ $paginator->nextPageUrl() }}" rel="nofollow" aria-label="">Загрузить еще</span>
        @endif
    </nav>
@endif
