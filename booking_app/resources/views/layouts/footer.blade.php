<footer>
    <div class="footer_wrapper">
        <div class="logo">
            @include('svg.icon_footer')
        </div>
        <div class="lf">
            <div class="city">
                Санкт-Петербург
            </div>
            <div class="title">
                GRAND - HOTEL
            </div>
            <div class="desc">
                Отель GRAND-HOTEL был основан в 1990 году и с тех пор приобрел репутацию одного из лучших отелей города. Мы предлагаем уникальное сочетание комфорта, уюта и изысканной кухни, что позволяет гостям насладиться своим пребыванием у нас.
            </div>
        </div>
        <div class="rt">
            <div class="links">
                <a href="{{route('about')}}" class="link">О нас</a>
                <a href="{{route('viewRooms')}}" class="link">Номера отеля</a>
                <a href="{{route('profile.bookings')}}" class="link">Бронирования</a>
            </div>
        </div>
    </div>
</footer>
