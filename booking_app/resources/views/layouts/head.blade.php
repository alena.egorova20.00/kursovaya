<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>@yield('title', __("seo." . Request::route()->getName() . ".title"))</title>
	<meta name="description" content="@yield('description', __("seo." . Request::route()->getName() . ".description"))">

<script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
	<link rel="icon" href="/favicon.svg" type="image/svg+xml">
	@vite([
		"resources/scss/layouts/common.scss",
		"resources/js/layouts/common.js",

		"resources/scss/". Request::route()->getName() .".scss",
		"resources/js/". Request::route()->getName() .".js"
	])

</head>
