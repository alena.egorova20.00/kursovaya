@extends('main')

@section('content')
    <div class="show">
        <div class="lf">
            <div class="title">
                {{ $room->title }}
            </div>
            <div class="desc">
                {{ $room->description }}
            </div>
            <div class="count_guest">
                Количество гостей: {{$room->count_guest}}
            </div>

            @if ($room->options)
                <div class="options">
                    @php
                        $options = json_decode($room->options);
                    @endphp

                    @foreach ($options as $optionId)
                        @php
                            $option = \App\Models\Option::find($optionId);
                        @endphp

                        @if ($option)
                            <div class="option">
                                @if ($option->name == 'Wi-fi')
                                    @include('svg.icon_wifi')
                                @elseif ($option->name == 'Кондиционер')
                                    @include('svg.icon_air_conditioner')
                                @endif
                                <span>{{ $option->name }}</span>
                            </div>
                        @endif
                    @endforeach
                </div>
            @endif
            <div class="price">
                {{ $room->room_type->price }} руб./ночь
            </div>
            @if ($room->status === 'available')
                <a href="{{ route('booking.create', ['room_id' => $room->id]) }}" class="booking-btn">Забронировать</a>
            @endif

        </div>

        <div class="rt">
            @if (!empty($room->gallery))
                @php
                    $gallery = json_decode($room->gallery, true);
                    $imagePath = asset('uploads/' . $room->id . '/' . $gallery[0]);
                @endphp
                <img src="{{ $imagePath }}" alt="Room Image">
            @else
                <p>No image available</p>
            @endif
        </div>
    </div>
@endsection
