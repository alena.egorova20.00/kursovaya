@extends('main')

@section('content')
    <div class="profile">
        <div class="lf">
            <div class="profile-nav">
                <a href="{{ route('profile.bookings') }}" class="active">БРОНИРОВАНИЯ</a>
                <a href="{{ route('profile.edit') }}" class="">ПРОФИЛЬ</a>
            </div>

            <div class="navs">
                <ul class="nav nav-tabs">
                    <li class="nav-item one">
                        <a class="nav-link active" id="tab1" data-toggle="tab" href="#tab-content1">
                            <span>ТЕКУЩЕЕ</span>
                        </a>
                    </li>
                    <li class="nav-item two">
                        <a class="nav-link" id="tab2" data-toggle="tab" href="#tab-content2">
                            <span>ПРОШЕДШИЕ</span>
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane fade show active" id="tab-content1">
                        @if ($bookings->count() > 0)
                            @foreach ($bookings as $key => $booking)
                                <div class="booking-item">
                                    <div class="item-lf">
                                        <div class="date">
                                            {{ \Carbon\Carbon::parse($booking->date_from)->format('d.m.Y') }}
                                            - {{ \Carbon\Carbon::parse($booking->date_to)->format('d.m.Y') }}
                                        </div>
                                        <div class="title">
                                            Название номера: {{ $booking->room->title }}
                                        </div>
                                        <div class="type">
                                            Тип номера: {{ $booking->room->room_type->name }}
                                        </div>
                                    </div>
                                    <div class="item-rt">
                                        <span class="booking-cancel"
                                              data-booking-id="{{ $booking->id }}">Отменить</span>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <span class="no-booking">Бронирований не найдено.</span>
                        @endif
                    </div>
                    <div class="tab-pane fade" id="tab-content2">
                        @if ($expiredBookings->count() > 0)
                            @foreach ($expiredBookings as $key => $booking)
                                <div class="booking-item">
                                    <div class="item-lf">
                                        <div class="date">
                                            {{ \Carbon\Carbon::parse($booking->date_from)->format('d.m.Y') }}
                                            - {{ \Carbon\Carbon::parse($booking->date_to)->format('d.m.Y') }}
                                        </div>
                                        <div class="type">
                                            Тип номера: {{ $booking->room->room_type->name }}
                                        </div>
                                    </div>
                                    {{-- <div class="item-rt">
                                        <span class="booking-cancel" data-booking-id="{{ $booking->id }}">Отменить</span>
                                    </div> --}}
                                </div>
                            @endforeach
                        @else
                            <span class="no-booking">Бронирований не найдено.</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="rt">
            <div class="personal-block">
                <div class="avatar-block">
                    <img src="{{ asset('storage/' . Auth::user()->avatar) }}" alt="">
                    <span class="personal-name">{{ Auth::user()->surname }} {{ Auth::user()->name }}</span>
                </div>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <button type="submit" class="logout">
                        Выйти
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
