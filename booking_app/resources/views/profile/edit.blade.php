@extends('main')

@section('content')
<div class="profile">
    <div class="lf">
        <div class="profile-nav">
            <a href="{{ route('profile.bookings') }}" class="">БРОНИРОВАНИЯ</a>
            <a href="{{ route('profile.edit') }}" class="active">ПРОФИЛЬ</a>
        </div>

        <form class="personal-form" action="{{ route('profile.update') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="row">
                <label for="">Фамилия</label>
                <input type="text" name="surname" value="{{ $user->surname }}" placeholder="Фамилия">
            </div>
            <div class="row">
                <label for="">Имя</label>
                <input type="text" name="name" value="{{ $user->name }}" placeholder="Имя">
            </div>
            <div class="row">
                <label for="">Отчество</label>
                <input type="text" name="patronymic" value="{{ $user->patronymic }}" placeholder="Отчество">
            </div>
            <div class="row">
                <label for="">Электронная почта</label>
                <input type="email" name="email" value="{{ $user->email }}" placeholder="email@mail.ru">
            </div>
            <div class="row">
                <label for="">Номер телефона</label>
                <input type="text" name="phone" value="{{ $user->phone }}" placeholder="+7 (900) 000 - 00 - 00">
            </div>

            <div class="row">
                <label for="">Аватарка</label>
                <input type="file" name="avatar" accept="image/*">
            </div>

            <button type="submit" class="submit">Изменить данные</button>
        </form>
    </div>
    <div class="rt">
        <div class="personal-block">
            <div class="avatar-block">
                <img src="{{ asset('storage/' . Auth::user()->avatar) }}" alt="">
                <span class="personal-name">{{ $user->surname }} {{ $user->name }}</span>
            </div>
            <form method="POST" action="{{ route('logout') }}">
                @csrf
                <button type="submit" class="logout">
                    Выйти
                </button>
            </form>

        </div>
    </div>
</div>
@endsection
