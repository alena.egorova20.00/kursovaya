@extends('main')

@section('content')
    <div class="booking-create">
        <div class="lf">
            <ul class="nav nav-tabs">
                <li class="nav-item one">
                    <a class="nav-link active" id="tab1" data-toggle="tab" href="#tab-content1">
                        <span>ПЕРСОНАЛЬНЫЕ ДАННЫЕ</span>
                    </a>
                </li>
                <li class="nav-item two">
                    <a class="nav-link" id="tab2" data-toggle="tab" href="#tab-content2">
                        <span>ПОДТВЕРЖДЕНИЕ</span>
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade show active" id="tab-content1">
                    <form class="form-create" id="form-create" action="{{ route('booking.store') }}" method="post">
                        @csrf
                        <input type="hidden" name="room_id" value="{{ $room->id }}">

                        <div class="row">
                            <label for="">ФИО</label>
                            <input type="text" name="full_name" placeholder="Иванов Иван Иванович">
                        </div>
                        <div class="row">
                            <label for="">Телефон</label>
                            <input type="text" name="phone" placeholder="+7 (900) 000 - 00 - 00">
                        </div>
                        <div class="row">
                            <label for="">Email</label>
                            <input type="email" name="email" placeholder="example@mail.ru">
                        </div>
                        <div class="row">
                            <label for="">Дата от</label>
                            <input type="date" name="date_from">
                        </div>
                        <div class="row">
                            <label for="">Дата до</label>
                            <input type="date" name="date_to">
                        </div>

                        <button class="submit" type="submit">Забронировать</button>
                    </form>
                </div>
                <div class="tab-pane fade" id="tab-content2">
                    <form class="form-confirm" id="form-confirm" action="{{ route('booking.confirm') }}" method="post">
                        @csrf

                        <div class="title">
                            Код подтверждения был отправлен на <br> адрес электронной почты <span id="return-email"></span>
                        </div>
                        <input type="text" name="confirmation_code" placeholder="Код подтверждения">
                        <button class="submit" type="submit">Подтвердить</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="rt">
            @if (!empty($room->gallery))
                @php
                    $gallery = json_decode($room->gallery, true);
                    $imagePath = asset('uploads/' . $room->id . '/' . $gallery[0]);
                @endphp
                <img src="{{ $imagePath }}" alt="Room Image">
            @else
                <p>No image available</p>
            @endif
            <div class="title">
                {{ $room->title }}
            </div>
        </div>
    </div>

    <div class="modal-overflow" id="booking-success">
        <div class="modal-booking-success">
            <span class="modal-close"></span>
            <div class="row">
                @include('svg.icon-success')
                <span>Номер успешно <br> забронирован!</span>
            </div>
        </div>
    </div>
@endsection
