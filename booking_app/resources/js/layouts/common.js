$(document).ready(function() {
    $('.modal-close').on('click', function(e) {
        $(this).closest('.modal-overflow').removeClass('show');
    });

    $('#login').on('click', function(e) {
        $('#modal-login').addClass('show');
    });

    $('.to-modal').on('click', function(e) {
        $('#modal-login').addClass('show');
    });



    $('#no-acc').on('click', function(e) {
        $('#modal-login').removeClass('show');
        $('#modal-reg').addClass('show');
    });

    $('#forgot_password').on('click', function(e) {
        $('#modal-login').removeClass('show');
        $('#modal-reg').removeClass('show');
        $('#pass_reset').addClass('show');
    });


    // Функция для валидации формы авторизации
    $('#modal-login form').submit(function(event) {
        event.preventDefault(); // Предотвращаем отправку формы по умолчанию
        var email = $(this).find('input[name="email"]').val();
        var password = $(this).find('input[name="password"]').val();
        // Проверяем введенные данные
        if (email.trim() === '') {
            alert('Введите адрес электронной почты.');
            return false;
        }
        if (password.trim() === '') {
            alert('Введите пароль.');
            return false;
        }
        // Если все в порядке, отправляем форму
        this.submit();
    });

    // Функция для валидации формы регистрации
    $('#modal-reg form').submit(function(event) {
        event.preventDefault(); // Предотвращаем отправку формы по умолчанию
        var surname = $(this).find('input[name="surname"]').val();
        var name = $(this).find('input[name="name"]').val();
        var phone = $(this).find('input[name="phone"]').val();
        var email = $(this).find('input[name="email"]').val();
        var password = $(this).find('input[name="password"]').val();
        var confirmPassword = $(this).find('input[name="password_confirmation"]').val();
        var agree = $(this).find('input[name="agree"]').is(':checked');
        // Проверяем введенные данные
        if (surname.trim() === '') {
            alert('Введите вашу фамилию.');
            return false;
        }
        if (name.trim() === '') {
            alert('Введите ваше имя.');
            return false;
        }
        if (phone.trim() === '') {
            alert('Введите ваш номер телефона.');
            return false;
        }
        if (email.trim() === '') {
            alert('Введите адрес электронной почты.');
            return false;
        }
        if (password.trim() === '') {
            alert('Введите пароль.');
            return false;
        }
        if (confirmPassword.trim() === '') {
            alert('Подтвердите пароль.');
            return false;
        }
        if (password !== confirmPassword) {
            alert('Пароли не совпадают.');
            return false;
        }
        if (!agree) {
            alert('Для регистрации необходимо согласиться с политикой конфиденциальности.');
            return false;
        }
        // Если все в порядке, отправляем форму
        this.submit();
    });

    // Функция для валидации формы восстановления доступа
    $('#pass_reset form').submit(function(event) {
        event.preventDefault(); // Предотвращаем отправку формы по умолчанию
        var email = $(this).find('input[name="email"]').val();
        // Проверяем введенные данные
        if (email.trim() === '') {
            alert('Введите адрес электронной почты.');
            return false;
        }
        // Если все в порядке, отправляем форму
        this.submit();
    });

});