$(document).ready(function() {
    $('.tab-content .tab-pane').not('.show.active').hide();

    $('.nav-tabs .nav-link').click(function(e) {
        e.preventDefault();

        var targetTab = $(this).attr('href');

        $('.tab-content .tab-pane').hide();

        $(targetTab).show();

        $('.nav-tabs .nav-link').removeClass('active');

        $(this).addClass('active');
    });

    $('.booking-cancel').on('click', function() {
        var bookingId = $(this).data('booking-id');

        var csrfToken = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            url: '/booking/' + bookingId + '/cancel',
            type: 'POST',
            data: {
                _token: csrfToken
            },
            success: function(response) {
                console.log(response);
                alert(response.success);
            },
            error: function(xhr) {
                console.log(xhr);
                alert('Произошла ошибка при отмене бронирования.');
            }
        });
    });



});