$(document).ready(function() {
    $(document).on('click', '.pagination-main .page-link', function(e) {
        e.preventDefault();

        var nextPageUrl = $(this).data('href');

        if (nextPageUrl) {
            $.ajax({
                url: nextPageUrl,
                method: 'GET',
                success: function(response) {
                    let _ = $(response);

                    // Добавляем новые элементы после существующих
                    $('.rooms_wrapper').append(_.find('.room'));

                    // Обновляем пагинацию
                    $('.pagination-block').html(_.find('.pagination-main'));

                    // Обновляем URL страницы в адресной строке
                    history.pushState({}, '', nextPageUrl);
                },
                error: function(xhr, status, error) {
                    console.error('Error:', error);
                }
            });
        }
    });
});