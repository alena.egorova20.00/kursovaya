$(document).ready(function() {
    $('#contact-form').submit(function(e) {
        e.preventDefault(); // Предотвращаем отправку формы по умолчанию

        // Получаем данные из формы
        var formData = $(this).serialize();

        // Отправляем AJAX-запрос на сервер
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: formData,
            success: function(response) {
                // Обработка успешного ответа от сервера
                console.log(response); // Выводим ответ в консоль (можно заменить на другую обработку)
                alert('Сообщение успешно отправлено!'); // Показываем пользователю уведомление об успешной отправке
            },
            error: function(xhr, status, error) {
                // Обработка ошибки
                console.error(xhr.responseText); // Выводим текст ошибки в консоль (можно заменить на другую обработку)
                alert('Произошла ошибка при отправке сообщения! Пожалуйста, попробуйте еще раз.'); // Показываем пользователю уведомление об ошибке
            }
        });
    });
});