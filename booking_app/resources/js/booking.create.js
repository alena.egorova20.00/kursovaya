$(document).ready(function() {
    $('.tab-content .tab-pane').not('.show.active').hide();

    $('.nav-tabs .nav-link').click(function(e) {
        e.preventDefault();

        var targetTab = $(this).attr('href');

        $('.tab-content .tab-pane').hide();

        $(targetTab).show();

        $('.nav-tabs .nav-link').removeClass('active');

        $(this).addClass('active');
    });

    // ---

    $(document).ready(function() {
        // Обработчик отправки первой формы
        $('#form-create').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function(response) {
                    console.log(response);

                    if (response.success) {
                        $('#return-email').text(response.email);
                        var targetTab = '#tab-content2';

                        $('.tab-content .tab-pane').hide();

                        $(targetTab).show();

                        $('.nav-tabs .nav-link').removeClass('active');

                        $('.nav-item.two .nav-link').addClass('active');
                    }
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                }
            });
        });

        $('#form-confirm').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function(response) {
                    console.log(response);
                    if (response.error) {
                        alert(response.error);
                    } else {
                        $('#booking-success').addClass('show');
                    }
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                    alert(xhr.responseText);
                }
            });
        });
    });
});